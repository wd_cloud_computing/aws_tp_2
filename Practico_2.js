/////////////////////////////////////////////////
// 1- Crear la tabla "Envios"
/////////////////////////////////////////////////
var params = {
    TableName: 'envio',
    KeySchema: [
        {
            AttributeName: 'id',
            KeyType: 'HASH',
        },
        {
            AttributeName: 'fechaAlta',
            KeyType: 'RANGE',
        }
    ],
    AttributeDefinitions: [ // The names and types of all primary and index key attributes only
        {
            AttributeName: 'id',
            AttributeType: 'S',
        },
        {
            AttributeName: 'fechaAlta',
            AttributeType: 'S',
        },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
    },
};
dynamodb.createTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

/////////////////////////////////////////////////
// 2 - Agregamos valores
/////////////////////////////////////////////////

//Usando put()
var params = {
    TableName: 'envio',
    Item: {
        id: '1',
        fechaAlta: new Date().toISOString(),
        destino: 'Las Heras 147, Godoy Cruz',
        email: 'nahue.martinez1987@gmail.com',
        pendiente: new Date().toISOString(),
    },
};
docClient.put(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

//Usando batchWrite()
var params = {
    RequestItems: {
        envio: [
            {
                PutRequest: {
                    Item: {
                      id: '2',
                      fechaAlta: new Date().toISOString(),
                      destino: 'Alvarez Thomas 724, Godoy Cruz',
                      email: 'maria.fernanda@gmail.com',
                      pendiente: new Date().toISOString(),
                    },
                },
            },
            {
                PutRequest: {
                    Item: {
                      id: '3',
                      fechaAlta: new Date().toISOString(),
                      destino: 'Rodriguez 723, Godoy Cruz',
                      email: 'utn.frm@gmail.com',
                    },
                },
            },
            {
                PutRequest: {
                    Item: {
                        id: '4',
                        fechaAlta: new Date().toISOString(),
                        destino: 'Vicente Zapata 273, Ciudad',
                        email: 'osep.central@gmail.com',
                    },
                }
            },
        ],
    },
};
docClient.batchWrite(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

/////////////////////////////////////////////////
// 3 - Leyendo los datos
/////////////////////////////////////////////////

var params = {
    TableName: 'envio',
};
dynamodb.scan(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

/////////////////////////////////////////////////
//4 - Buscamos un elemento usando Query
/////////////////////////////////////////////////

var params = {
   TableName: 'envio',
   KeyConditionExpression: 'id = :value',
   ExpressionAttributeValues: {
     ':value': '2',
   },
};
docClient.query(params, function(err, data) {
   if (err) ppJson(err); // an error occurred
   else ppJson(data); // successful response
});

var params = {
    TableName: 'envio',
    KeyConditionExpression: 'id = :value AND fechaAlta = :fecha',
    ExpressionAttributeValues: {
      ':value': '2',
      ':fecha': "2018-09-04T21:33:19.962Z",
    },
};
docClient.query(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});


/////////////////////////////////////////////////
//5 - Creamos un índice
/////////////////////////////////////////////////
var params = {
    TableName: 'envio',
    AttributeDefinitions: [
        {
            AttributeName: 'pendiente',
            AttributeType: 'S',
        },
    ],
    GlobalSecondaryIndexUpdates: [{ // optional
            Create: {
                IndexName: 'envio-pendiente',
                KeySchema:[
                    {
                        AttributeName: 'id',
                        KeyType: 'HASH',
                    },
                    {
                        AttributeName: 'pendiente',
                        KeyType: 'RANGE',
                    },
                ],
                Projection: {
                    ProjectionType: 'KEYS_ONLY'
                },
                ProvisionedThroughput: {
                    ReadCapacityUnits: 1,
                    WriteCapacityUnits: 1,
                },
            },
        },
    ],
    //ProvisionedThroughput: {
    //    ReadCapacityUnits: 0,
    //    WriteCapacityUnits: 0,
    //},
};
dynamodb.updateTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});



/////////////////////////////////////////////////
//6 - Agregamos un historial
/////////////////////////////////////////////////

var params = {
    TableName: 'envio',
    Item: {
        id: '2',
        fechaAlta: '2018-09-04T21:33:19.962Z',
        destino: 'Las Heras 147, Godoy Cruz',
        email: 'nahue.martinez1987@gmail.com',
        pendiente: '2018-09-04T21:33:19.962Z',
        historial: [
          {
            fecha: new Date().toISOString(),
            descripcion: 'Aceptado en centro de distribucion'
          }
        ],
    },
};
docClient.put(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

//Consultamos
var params = {
    TableName: 'envio',
};
dynamodb.scan(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
